package server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class Processor extends Thread{
	
	private InputStream in;
	private PrintStream out;
	private final static String WEB_ROOT = "E:/Study/doc";

	public Processor(Socket socket){
		try {
			// 获取socket的输入流
			in = socket.getInputStream();
			// 获取socket的输出流
			out = new PrintStream(socket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run(){
		System.out.println("====== Server run ======");
		String fileName = parse(in);
		sendFile(fileName);
	}
	
	/**
	 * 获取文件名
	 * @param in
	 * @return
	 */
	public String parse(InputStream in){
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String fileName = "";
		try {
			String httpMessage = br.readLine();
			// 有三部分，协议的状态号，请求文件的名称，请求协议版本号
			String[] content = httpMessage.split(" ");
			
			if(content.length != 3){
				sendErrorMessage(400, "client request error!");
				return null;
			}
			
			System.out.println("code:" + content[0] + " fileName:" + content[1] + " http version:" + content[2]);
			fileName = content[1];
		} catch (IOException e) {
			e.printStackTrace();
		}
		return fileName;
	}
	
	/**
	 * 返回错误信息
	 * @param errorCode
	 * @param errorMessage
	 */
	public void sendErrorMessage(int errorCode, String errorMessage){
		out.println("HTTP/1.0 " + errorCode + " " + errorMessage);
		out.println("content-type: text/html; charset=utf-8");
		out.println();
		out.println("<html>");
		out.println("<title>Error</title>");
		out.println("<body>");
		out.println("<h1>ErrorCode: " + errorCode + "</h1>");
		out.println("<h1>ErrorMessage: " + errorMessage + "</h1>");
		out.println("</body>");
		out.println("</html>");
		out.flush();
		out.close();
		try {
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 返回对应的文件
	 * @param fileName
	 */
	public void sendFile(String fileName){
		File file = new File(Processor.WEB_ROOT + fileName);
		// 请求资源不存在
		if(!file.exists()){
			sendErrorMessage(404, "File Not Found!");
			return;
		}
		try {
			InputStream in = new FileInputStream(file);
			byte[] content = new byte[(int) file.length()];
			in.read(content);
			out.println("HTTP/1.0 200 queryfile");
			out.println("content-lenght:" + content.length);
			out.println();
			out.write(content);
			out.flush();
			out.close();
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
