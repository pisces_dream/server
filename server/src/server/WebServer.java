package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 
 * @author Lihm
 * server-WebServer.java
 * 2015年1月22日 下午9:06:20
 *
 */
public class WebServer {

	/**
	 * 启动服务器，对端口进行监听
	 * @param port
	 */
	public void startServer(int port) {
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(port);
			while (true) {
				System.out.println("====== Listens for a connection ======");
				Socket socket = serverSocket.accept();
				new Processor(socket).start();
				System.out.println("====== Listens End ======");
			}
		} catch (IOException e) {
			try {
				serverSocket.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		int port = 8088;
		if (args.length == 1) {
			port = Integer.parseInt(args[0]);
		}
		new WebServer().startServer(port);
	}

}
